salle(amphi1,350).
salle(amphi2,160).
salle(d117,24).
salle(b001,15).
salle(c002,13).
salle(d004,65).
salle(d012,24).

creneau(1,8,9.5).
creneau(2,9.75,11.25).
creneau(3,11.5,13).
creneau(4,14,15.5).
creneau(5,15.75,17.25).
creneau(6,17.5,18.75).
creneau(7,8,9.5).
creneau(8,9.75,11.25).
creneau(9,11.5,13).
creneau(10,14,15.5).
creneau(11,15.75,17.25).
creneau(12,17.5,18.75).
creneau(13,8,9.5).
creneau(14,9.75,11.25).
creneau(15,11.5,13).
creneau(16,14,15.5).
creneau(17,15.75,17.25).
creneau(18,17.5,18.75).
creneau(19,8,9.5).
creneau(20,9.75,11.25).
creneau(21,11.5,13).
creneau(22,14,15.5).
creneau(23,15.75,17.25).
creneau(24,17.5,18.75).
creneau(25,8,9.5).
creneau(26,9.75,11.25).
creneau(27,11.5,13).
creneau(28,14,15.5).
creneau(29,15.75,17.25).
creneau(30,17.5,18.75).

enseignant(marc_gelgon).
enseignant(jean-pierre_guedon).
enseignant(hoel_le_capitaine)
enseignant(guillaume_raschia).
enseignant(remi_lehn).
enseignant(florian_leman).
enseignant(pascale_kuntz).
enseignant(philippe_peter).
enseignant(matthieu_perreira_da_silva).

matiere(intelligence_artificielle).
matiere(traitement_d_image).
matiere(reseaux_3).
matiere(optimisation_et_meta_heuristiques).
matiere(reunion).

groupe(info).
groupe(id).
groupe(silr).
groupe(silr1).
groupe(silr2).
groupe(silrp).
groupe(silrc).

incompatible(info,info).
incompatible(info,id).
incompatible(info,silr).
incompatible(info,silr1).
incompatible(info,silr2).
incompatible(info,silrp).
incompatible(info,silrc).
incompatible(id,id).
incompatible(silr,silr).
incompatible(silr,silr1).
incompatible(silr,silr2).
incompatible(silr,silrp).
incompatible(silr,silrc).
incompatible(silr1,silr1).
incompatible(silr1,silrp).
incompatible(silr1,silrc).
incompatible(silr2,silr2).
incompatible(silr2,silrp).
incompatible(silr2,silrc).
incompatible(silrp,silrp).
incompatible(silrp,silrc).
incompatible(silrc,silrc).

enseigne(marc_gelgon, traitement_d_image).
enseigne(jean_pierre_guedon, traitement_d_image).
enseigne(guillaume_raschia, intelligence_artificielle).
enseigne(hoel_le_capitaine, intelligence_artificielle)
enseigne(remi_lehn, reseaux_3).
enseigne(florian_leman, réseaux_3).
enseigne(pascale_kuntz, optimisation_et_meta_heuristiques).
enseigne(philippe_peter, optimisation_et_meta_heuristiques).
enseigne(matthieu_perreira_da_silva,reunion).

heberge(amphi1,cm).
heberge(amphi1,ds).
heberge(amphi2,cm).
heberge(amphi2,ds).
heberge(c002,tp).
heberge(d004,cm).
heberge(d012,tp).